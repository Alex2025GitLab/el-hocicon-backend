import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";
import { jwtVerify } from "jose";
import { TOKEN, PUBLIC } from "./types/strings";

export async function middleware(request: NextRequest) {
  const jwt = request.cookies.get(TOKEN);
  if (!jwt) {
    return NextResponse.redirect(new URL(`${PUBLIC}/login`, request.url));
  }

  try {
    const { payload } = await jwtVerify(
      jwt.value,
      new TextEncoder().encode(process.env.JWT_SECRET)
    );
    return NextResponse.next();
  } catch (error) {
    return NextResponse.redirect(new URL(`${PUBLIC}/login`, request.url));
  }
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: ["/admin/:path*"],
};

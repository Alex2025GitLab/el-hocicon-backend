import { AxiosError } from "axios";

// For Datatable
// align right para números
export interface Column {
  id: string;
  label: string;
  minWidth?: number;
  isImage?: boolean;
  isDate?: boolean;
  align?: "right";
  format?: (value: number) => string;
}

// "2024-01-12T20:38:13Z"
export function formatDate(dateString: string) {
  const options: any = { year: "numeric", month: "long", day: "numeric" };
  return new Date(dateString).toLocaleDateString(undefined, options);
}
export function getAxiosError(errorData: AxiosError) {
  let messageError = "";
  try {
    // @ts-ignore
    const error = errorData.response?.data?.error;
    messageError = error;
  } catch (e) {
    messageError = "Ups... ocurrió un error inesperado";
  }
  return messageError;
}

export function convertirFormatoDashboard(fechaString: string) {
  // Crear un objeto Date a partir del string de fecha
  var fecha = new Date(fechaString);

  // Obtener los componentes de la fecha
  var dia: string | number = fecha.getUTCDate();
  var mes: string | number = fecha.getUTCMonth() + 1; // Los meses son indexados desde 0
  var anio: string | number = fecha.getUTCFullYear();
  var horas: string | number = fecha.getUTCHours();
  var minutos: string | number = fecha.getUTCMinutes();

  // Formatear los componentes para que tengan dos dígitos si es necesario
  dia = dia < 10 ? "0" + dia : dia;
  mes = mes < 10 ? "0" + mes : mes;
  horas = horas < 10 ? "0" + horas : horas;
  minutos = minutos < 10 ? "0" + minutos : minutos;

  // Construir la cadena en el formato deseado
  var formatoFinal = dia + "/" + mes + "/" + anio + " " + horas + ":" + minutos;

  return formatoFinal;
}

export function isNumber(value: any): boolean {
  return typeof value === "number" && !isNaN(value);
}

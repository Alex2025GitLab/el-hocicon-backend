## **_Este es el repositorio FRONTEND de servidor (disculpe la molestia) Next._**

# Instalación

## Requerimientos del servidor

- NodeJS version 20
- Estar ejecutandose el BACKEND
- **Puertos que deben estar libres**: 3001 (Aquí se ejecuta el frontend)

## Instalación del proyecto

1. Clonar el código fuente desde el repositorio.

```sh
$ git clone https://gitlab.com/Alex2025GitLab/el-hocicon-backend.git
```

2. Ingresar a la carpeta.

```sh
$ cd el-hocicon-backend
```

3. Instalar las dependencias de paquetes npm

```sh
$ npm install
```

## Configuraciones

1. Copiar archivos de configuración y modificarlos según necesidad (**no se recomienda modificar**).

```sh
$ cp .env.sample .env.local
```

2. Dejar el env configurado como está ya que comparte variables con el backend

## Iniciar el Frontend

1. Para ejecutar el servidor debe de ejecutar

```sh
$ npm run dev
```

## Sobre produccón

Lamentablemente no me alcanzó el tiempo para revisar el estado de la build ya que presenta fallas que no pude resolver, este proyecto solo se puede ejecutar en desarrollo

## Sobre el uso de api/route desde Nextjs

Se usó una ruta de api de Nextjs simplemente fue para la creación del token JWT para el cliente como cookie; las demás APIs se ejecutan desde el BACKEND y el propio BACKEND crea el JWT token

## Sobre la responsividad

Disculpe que no sea responsivo en su totalidad no me alcanzó el tiempo

## Sobre las cuentas de usuario con password en hash

todas las cuentas: aaalex2025@gmail.com y seed1@gmail.com hast seed4@gmail.com tiene la contraseña de **Alex2025** es necesario iniciar sesión para ingresar a **/admin** ruta de frontend

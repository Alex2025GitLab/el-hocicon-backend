import { TypographyPropsVariantOverrides } from "@mui/material";
import { Merriweather, Roboto } from "next/font/google";

const roboto = Roboto({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
  display: "swap",
});
const merriweather = Merriweather({
  weight: ["300", "400", "700", "900"],
  style: ["normal", "italic"],
  subsets: ["latin"],
});

const themeTypography: TypographyPropsVariantOverrides = {
  fontFamily: `${roboto.style.fontFamily},${merriweather.style.fontFamily}, SF, sans-serif`,
  htmlFontSize: 16,
  fontSize: 16,
  fontWeight: 400,
  caption: {
    fontSize: "0.8rem",
    fontWeight: 400,
  },
  body2: {
    fontSize: "0.95rem",
    fontWeight: 400,
  },
  body1: {
    fontSize: "1.15rem",

    fontWeight: 400,
    lineHeight: 1.2,
  },
  h6: {
    fontSize: "1.25rem",
    fontWeight: 400,
    lineHeight: 1.2,
  },
  h5: {
    fontSize: "1.35rem",
    fontWeight: 400,
    lineHeight: 1.2,
  },
  h4: {
    fontSize: "1.75rem",
    "@media (max-width: 900px)": {
      fontSize: "1.2rem",
    },
    fontWeight: 500,
    lineHeight: 1.2,
  },
  h3: {
    fontSize: "1.6rem",
    "@media (min-width: 900px)": {
      fontSize: "2.2rem",
    },
    fontWeight: 500,
    lineHeight: 1.2,
  },
  h2: {
    fontSize: "3rem",
    fontWeight: 500,
    lineHeight: 1.2,
  },
  h1: {
    fontSize: "4rem",
    fontWeight: 700,
    lineHeight: 1.2,
  },
  button: {
    textTransform: "none",
  },
  commonAvatar: {
    cursor: "pointer",
    borderRadius: "8px",
  },
  smallAvatar: {
    width: "22px",
    height: "22px",
    fontSize: "1rem",
  },
  mediumAvatar: {
    width: "34px",
    height: "34px",
    fontSize: "1.2rem",
  },
  largeAvatar: {
    width: "44px",
    height: "44px",
    fontSize: "1.5rem",
  },
};

export default themeTypography;

"use client";
import { createTheme } from "@mui/material/styles";
import typography from "./typography";

const theme = createTheme({
  palette: {
    mode: "light",
    primary: {
      main: "#E50A14",
    },
    background: {
      default: "#FFFFFF",
    },
    text: {
      primary: "#151822",
      secondary: "#3B3F47",
    },
  },
  typography,
});
export default theme;

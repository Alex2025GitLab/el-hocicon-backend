import * as React from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Sections1 from "./components/landing/Sections1";
import { Container, Stack } from "@mui/material";
import Navbar from "./components/landing/Navbar";
import Latest from "./components/landing/Latest";
import { Article } from "@/types/news.interface";
import { v4 as uuidv4 } from "uuid";
import LatestNews from "./components/landing/LatestNews";
import Footer from "./components/landing/Footer";
import Subscribe from "./components/landing/Subscribe";
import MustRead from "./components/landing/MustRead";
import { API_LANDING } from "@/types/strings";

interface LandingData {
  latest: Article | undefined;
  latestNews: Article[];
}

const getLandigData = async (): Promise<LandingData> => {
  const res = await fetch(API_LANDING, { next: { revalidate: 0 } });
  if (!res.ok) {
    return res.text().then((text) => {
      throw new Error(text);
    });
  }
  const data = await res.json();
  return data;
};

export default async function HomePage() {
  const landingData: LandingData = await getLandigData();
  return (
    <Container>
      <Navbar isMain={true} />
      {/* Sections */}
      <Container sx={{ mt: 12 }}>
        <Stack gap={5}>
          <Sections1 />
          {landingData.latest !== undefined && (
            <Latest
              author={landingData.latest.author}
              description={landingData.latest.description}
              placePublication={landingData.latest.placePublication}
              publishedAt={landingData.latest.publishedAt}
              title={landingData.latest.title}
              authorImg={landingData.latest.authorImg}
              imageUrl={landingData.latest.imageUrl}
            />
          )}

          <LatestNews articles={landingData.latestNews.slice(0, 4)} />
          <Subscribe />
          <MustRead />

          <Footer />
        </Stack>
      </Container>
    </Container>
  );
}

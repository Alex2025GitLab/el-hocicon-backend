import React, { forwardRef, ReactNode, Ref } from "react";

// material-ui
import { useTheme, Theme } from "@mui/material/styles";
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Typography,
  SxProps,
} from "@mui/material";

// Props for the MainCard component
interface MainCardProps {
  border?: boolean;
  boxShadow?: boolean;
  children?: ReactNode;
  content?: boolean;
  contentClass?: string;
  contentSX?: SxProps;
  darkTitle?: boolean;
  secondary?: ReactNode | string | object;
  shadow?: string;
  sx?: SxProps;
  title?: ReactNode | string | object;
}

// constant
const headerSX: SxProps = {
  "& .MuiCardHeader-action": { mr: 0 },
};

// ==============================|| CUSTOM MAIN CARD ||============================== //
const MainCard = ({
  border = true,
  boxShadow,
  children,
  content = true,
  contentClass = "",
  contentSX = {},
  darkTitle,
  secondary,
  shadow,
  sx = {},
  title,
  ...others
}: MainCardProps) => {
  const theme: any = useTheme() as Theme;

  return (
    <Card
      variant="outlined"
      {...others}
      sx={{
        borderColor: theme.palette.primary[200] + 25,
        ":hover": {
          boxShadow: boxShadow
            ? shadow || "0 2px 14px 0 rgb(32 40 45 / 8%)"
            : "inherit",
        },
        ...sx,
      }}
    >
      {/* card header and action */}
      {
        <CardHeader
          sx={headerSX}
          title={
            darkTitle ? (
              <Typography variant="h3">{title as string}</Typography>
            ) : (
              (title as string)
            )
          }
          action={secondary as any}
        />
      }

      {/* content & header divider */}
      {title && <Divider />}

      {/* card content */}
      {content && (
        <CardContent sx={contentSX} className={contentClass}>
          {children}
        </CardContent>
      )}
      {!content && children}
    </Card>
  );
};

export default MainCard;

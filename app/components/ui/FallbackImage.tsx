"use client";
import { useTheme } from "@mui/material/styles";
import { Card, Box, Stack, Typography } from "@mui/material";
import React from "react";
import Image from "next/image";

const FallbackImage = () => {
  const theme = useTheme();
  return (
    <Box
      sx={{
        display: "grid",
        placeItems: "center",
        width: "100%",
        height: "100%",
        backgroundColor: "#E3E3E3",
        color: theme.palette.primary.main,
      }}
    >
      <Stack spacing={3} alignItems="center">
        <Image
          src="/images/news.svg"
          alt="article fallback"
          width={50}
          height={50}
        />
        <Typography>El Hocicón</Typography>
      </Stack>
    </Box>
  );
};

export default FallbackImage;

import {
  Menu,
  NewspaperOutlined,
  NotificationsOutlined,
  Settings,
} from "@mui/icons-material";
import {
  Avatar,
  Box,
  ButtonBase,
  Chip,
  Stack,
  Typography,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import Link from "next/link";
import React from "react";

const AdminHeader = () => {
  const theme = useTheme();

  return (
    <>
      {/* logo & toggler button */}
      <Box
        sx={{
          width: 228,
          display: "flex",
          [theme.breakpoints.down("md")]: {
            width: "auto",
          },
        }}
      >
        <Box
          component="span"
          sx={{ display: { xs: "none", md: "block" }, flexGrow: 1 }}
        >
          <Link href="/">
            <Stack
              sx={{ color: theme.palette.primary.main }}
              direction="row"
              alignItems="center"
              spacing={1}
            >
              <NewspaperOutlined />
              <Typography
                variant="h4"
                sx={{
                  fontFamily: "Merriweather",
                  fontWeight: 700,
                }}
              >
                El&nbsp;Hocicón
              </Typography>
            </Stack>
          </Link>
        </Box>
        <ButtonBase sx={{ borderRadius: "12px", overflow: "hidden" }}>
          <Avatar
            variant="rounded"
            sx={{
              transition: "all .2s ease-in-out",
              background: "#ffd7df",
              color: theme.palette.primary.main,
              "&:hover": {
                background: theme.palette.primary.main,
                color: "#ffd7df",
              },
            }}
            color="inherit"
          >
            <Menu />
          </Avatar>
        </ButtonBase>
      </Box>

      <Box sx={{ flexGrow: 1 }} />
      <Box sx={{ flexGrow: 1 }} />

      {/* notification & profile */}
      <NotificationSection />
      <ProfileSection />
    </>
  );
};

const ProfileSection = () => {
  const theme = useTheme();
  return (
    <Chip
      sx={{
        height: "48px",
        alignItems: "center",
        borderRadius: "27px",
        transition: "all .2s ease-in-out",
        borderColor: "#EEF2F6",
        backgroundColor: "#EEF2F6",
        '&[aria-controls="menu-list-grow"], &:hover': {
          borderColor: theme.palette.primary.main,
          background: `#ffeef1!important`,
          color: theme.palette.primary.light,
          "& svg": {
            stroke: theme.palette.primary.light,
          },
        },
        "& .MuiChip-label": {
          lineHeight: 0,
        },
      }}
      icon={
        <Avatar
          src="https://mdbcdn.b-cdn.net/img/new/avatars/3.webp"
          sx={{
            margin: "8px 0 8px 8px !important",
            cursor: "pointer",
          }}
          aria-controls="menu-list-grow"
          aria-haspopup="true"
          color="inherit"
        />
      }
      label={<Settings sx={{ color: theme.palette.primary.main }} />}
      variant="outlined"
      aria-controls={"menu-list-grow"}
      aria-haspopup="true"
      color="primary"
    />
  );
};

const NotificationSection = () => {
  const theme = useTheme();
  return (
    <>
      <Box
        sx={{
          ml: 2,
          mr: 3,
          [theme.breakpoints.down("md")]: {
            mr: 2,
          },
        }}
      >
        <ButtonBase sx={{ borderRadius: "12px" }}>
          <Avatar
            variant="rounded"
            sx={{
              borderRadius: 3,
              transition: "all .2s ease-in-out",
              background: "#ffd7df",
              color: theme.palette.primary.main,
              "&:hover": {
                background: theme.palette.primary.main,
                color: "#ffd7df",
              },
            }}
            aria-controls="menu-list-grow"
            aria-haspopup="true"
            color="inherit"
          >
            <NotificationsOutlined />
          </Avatar>
        </ButtonBase>
      </Box>
    </>
  );
};

export default AdminHeader;

"use client";
import { Close, Search } from "@mui/icons-material";
import { useState } from "react";
import {
  Box,
  Button,
  Divider,
  Grid,
  Hidden,
  IconButton,
  InputBase,
  Paper,
  Stack,
  Typography,
} from "@mui/material";
import { Title, Group, Place } from "@mui/icons-material";
import React from "react";
import { Filters } from "@/types/news.interface";

interface SearchHeader {
  query: string;
  onQuery: Function;
  filter: Filters;
  onSearch: Function;
  onFilter: Function;
}
const SearchHeader = ({
  query,
  onQuery,
  filter,
  onFilter,
  onSearch,
}: SearchHeader) => {
  return (
    <Grid container gap={3}>
      <Grid item>
        <Stack direction="row" gap={3}>
          <Paper
            component="form"
            variant="outlined"
            sx={{
              p: "1px 2px",
              display: "flex",
              alignItems: "center",
              width: 300,
              "@media (min-width: 900px)": {
                width: 400,
              },
              height: 40,
              borderRadius: 2,
              borderColor: "#E50A14",
              paddingX: 1,
            }}
          >
            <Search sx={{ color: "gray" }} />
            <InputBase
              sx={{ ml: 1, flex: 1, fontSize: 15 }}
              placeholder="Buscar Noticia"
              inputProps={{ "aria-label": "buscar noticia" }}
              value={query}
              onChange={(e) => onQuery(e.target.value)}
            />
            {query.length > 0 && (
              <IconButton onClick={() => onQuery("")}>
                <Close />
              </IconButton>
            )}
          </Paper>

          <Button
            onClick={() => {
              if (query.length > 0) onSearch();
            }}
            variant={query.length > 1 ? "contained" : "outlined"}
          >
            <Typography variant="caption">Buscar</Typography>
          </Button>
        </Stack>
      </Grid>
      <Grid item>
        <Stack direction="row" alignItems="center" gap={1}>
          <Button
            variant={filter.title ? "contained" : "outlined"}
            sx={{ transition: "all ease-in 0.3s" }}
            onClick={() =>
              onFilter({
                ...filter,
                title: !filter.title,
              })
            }
            startIcon={<Title />}
          >
            <Typography variant="body2">Titulo</Typography>
          </Button>
          <Button
            variant={filter.autor ? "contained" : "outlined"}
            sx={{ transition: "all ease-in 0.3s" }}
            onClick={() =>
              onFilter({
                ...filter,
                autor: !filter.autor,
              })
            }
            startIcon={<Group />}
          >
            <Typography variant="body2">Autor</Typography>
          </Button>
          <Button
            variant={filter.place ? "contained" : "outlined"}
            sx={{ transition: "all ease-in 0.3s" }}
            onClick={() =>
              onFilter({
                ...filter,
                place: !filter.place,
              })
            }
            startIcon={<Place />}
          >
            <Typography variant="body2">Lugar</Typography>
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};

interface Filter {
  name: string;
  Icon: any;
}
const Filter = ({ name, Icon }: Filter) => {
  const [isClicked, setIsClicked] = useState(false);
  return (
    <Button
      variant={isClicked ? "outlined" : "contained"}
      sx={{ transition: "all ease-in 0.3s" }}
      onClick={() => setIsClicked(!isClicked)}
    >
      <IconButton>
        <Icon />
      </IconButton>
      <Typography>{name}</Typography>
    </Button>
  );
};

export default SearchHeader;

"use client";
import { Article } from "@/types/news.interface";
import {
  Stack,
  Typography,
  Box,
  Card,
  Grid,
  Avatar,
  Divider,
  Hidden,
} from "@mui/material";
import React from "react";
import FallbackImage from "../ui/FallbackImage";
import deepOrange from "@mui/material/colors/deepOrange";
import { formatDate } from "@/utils/utils";
import { useTheme } from "@mui/material/styles";
import Image from "next/image";
import { API_PUBLIC } from "@/types/strings";

interface SearchBody {
  query: string;
  articles: Article[];
}
const SearchBody = ({ query, articles }: SearchBody) => {
  return (
    <Stack gap={3}>
      {/* <Box>
        {query.length > 0 ? (
          <Typography variant="body2">
            Mostrando resultados de&nbsp;
            <span style={{ fontWeight: "bold" }}>{query}</span>
          </Typography>
        ) : (
          <Typography variant="body2">Últimas noticias</Typography>
        )}
      </Box> */}
      <Box>
        <Typography variant="body2">Resultados...</Typography>
      </Box>
      {articles.length > 0 ? (
        <Stack gap={5}>
          {articles.map((elem) => (
            <SearchItem
              key={elem.id}
              author={elem.author}
              description={elem.description}
              placePublication={elem.placePublication}
              publishedAt={elem.publishedAt}
              title={elem.title}
              authorImg={elem.authorImg}
              imageUrl={elem.imageUrl}
            />
          ))}
        </Stack>
      ) : (
        <Box sx={{ display: "grid", placeItems: "center", width: "100%" }}>
          <Typography>No se encontraron coincidencias</Typography>
        </Box>
      )}
    </Stack>
  );
};

const SearchItem = ({
  title,
  imageUrl,
  publishedAt,
  placePublication,
  author,
  description,
  authorImg,
}: Article) => {
  const theme = useTheme();
  return (
    <Grid container gap={2} wrap="nowrap">
      <Grid item xs={6}>
        <Stack gap={1}>
          <Card sx={{ maxHeight: "200px" }}>
            {imageUrl && imageUrl.length > 0 ? (
              <Image
                width={200}
                height={200}
                src={`${API_PUBLIC}/${imageUrl}`}
                style={{ objectFit: "cover", width: "100%", height: "100%" }}
                alt={
                  title.length > 5
                    ? `image_${title.substring(0, 5)}`
                    : "image for article"
                }
              />
            ) : (
              <Box sx={{ width: "100%", height: 200 }}>
                <FallbackImage />
              </Box>
            )}
          </Card>
          <Hidden mdUp>
            {/* Place publication */}
            <Stack direction="row" gap={1}>
              <Typography
                variant="caption"
                sx={{ color: theme.palette.primary.main, fontSize: 12 }}
              >
                {placePublication}
              </Typography>
              <Divider orientation="vertical" flexItem />
              <Typography sx={{ fontSize: 12 }} variant="caption">
                {publishedAt.length > 0 ? formatDate(publishedAt) : "0/0/1900"}
              </Typography>
            </Stack>
          </Hidden>
        </Stack>
      </Grid>
      <Grid item xs={6}>
        <Stack direction="column" gap={1}>
          <Stack direction="row" gap={1}>
            {authorImg && authorImg.length > 0 ? (
              <Avatar
                sx={{ width: 24, height: 24 }}
                alt={`author_${author}`}
                src={`${API_PUBLIC}/${authorImg}`}
              />
            ) : (
              <Avatar sx={{ bgcolor: deepOrange[800], width: 24, height: 24 }}>
                H
              </Avatar>
            )}
            <Typography variant="caption">{author}</Typography>
          </Stack>

          <Typography
            variant="h5"
            sx={{ fontFamily: "Merriweather", fontWeight: 700 }}
          >
            {title}
          </Typography>

          <Typography
            variant="caption"
            sx={{ color: theme.palette.text.secondary }}
          >
            {description}
          </Typography>

          <Hidden mdDown>
            {/* Place publication */}
            <Stack direction="row" gap={1}>
              <Typography
                variant="caption"
                sx={{ color: theme.palette.primary.main, fontSize: 12 }}
              >
                {placePublication}
              </Typography>
              <Divider orientation="vertical" flexItem />
              <Typography sx={{ fontSize: 12 }} variant="caption">
                {publishedAt.length > 0 ? formatDate(publishedAt) : "0/0/1900"}
              </Typography>
            </Stack>
          </Hidden>
        </Stack>
      </Grid>
    </Grid>
  );
};

export default SearchBody;

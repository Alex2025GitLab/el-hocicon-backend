"use client";
import { Article } from "@/types/news.interface";
import {
  Container,
  Stack,
  Card,
  Box,
  Typography,
  Grid,
  Avatar,
  Divider,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import Image from "next/image";
import React from "react";
import FallbackImage from "../ui/FallbackImage";
import { deepOrange } from "@mui/material/colors";
import { formatDate } from "@/utils/utils";
import { API_PUBLIC } from "@/types/strings";

const Latest = ({
  id,
  title,
  imageUrl,
  publishedAt,
  placePublication,
  author,
  description,
  authorImg,
}: Article) => {
  const theme = useTheme();

  return (
    <Grid container>
      <Grid item xs={12} md={6}>
        <Card
          variant="elevation"
          sx={{
            borderRadius: 4,
            marginRight: 0,
            maxHeight: "230px",
            "@media (min-width: 900px)": { marginRight: 3, maxHeight: "300px" },
          }}
        >
          {imageUrl && imageUrl?.length > 0 ? (
            <Image
              width={400}
              height={300}
              src={`${API_PUBLIC}/${imageUrl}`}
              style={{
                objectFit: "cover",
                width: "100%",
                height: "100%",
              }}
              alt={
                title.length > 5
                  ? `image_${title.substring(0, 5)}`
                  : "image for article"
              }
            />
          ) : (
            <Box sx={{ width: "100%", height: 300 }}>
              <FallbackImage />
            </Box>
          )}
        </Card>
      </Grid>

      <Grid item xs={12} md={6}>
        <Stack
          direction="column"
          sx={{
            marginLeft: 0,
            marginTop: 2,
            "@media (min-width: 900px)": { marginLeft: 3 },
          }}
          spacing={1}
        >
          <Stack direction="row" spacing={2}>
            <Stack direction="row" spacing={1}>
              {authorImg && authorImg.length > 0 ? (
                <Avatar
                  sx={{ width: 24, height: 24 }}
                  alt={`author_${author}`}
                  src={`${API_PUBLIC}/${authorImg}`}
                />
              ) : (
                <Avatar
                  sx={{ bgcolor: deepOrange[800], width: 24, height: 24 }}
                >
                  H
                </Avatar>
              )}
              <Typography variant="caption">{author}</Typography>
            </Stack>
            <Divider orientation="vertical" flexItem />
            <Typography variant="caption">
              {publishedAt.length > 0 ? formatDate(publishedAt) : "0/0/1900"}
            </Typography>
          </Stack>

          <Typography
            variant="h3"
            sx={{ fontFamily: "Merriweather", fontWeight: 700 }}
          >
            {title}
          </Typography>

          <Typography
            variant="caption"
            sx={{ color: theme.palette.text.secondary }}
          >
            {description}
          </Typography>

          {/* Place publication */}
          <Typography sx={{ color: theme.palette.primary.main }}>
            {placePublication}
          </Typography>
        </Stack>
      </Grid>
    </Grid>
  );
};

export default Latest;

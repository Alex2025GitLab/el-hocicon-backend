"use client";
import { useTheme } from "@mui/material/styles";
import { Card, Container, Grid, Stack, Typography } from "@mui/material";
import React from "react";
import LinkButton from "../ui/buttons/LinkButton";
import { ArrowRightAlt } from "@mui/icons-material";
import { Article } from "@/types/news.interface";
import LatestArticle from "./LatestArticle";

const LatestNews = ({ articles }: { articles: Article[] }) => {
  const theme = useTheme();
  return (
    <Stack gap={3}>
      <Stack direction="row" justifyContent="space-between">
        <Typography variant="h4" sx={{ fontWeight: 700 }}>
          Últimas noticias
        </Typography>
        <LinkButton sx={{ color: theme.palette.primary.main }}>
          <Typography>Ver más</Typography>
          <ArrowRightAlt />
        </LinkButton>
      </Stack>

      <Grid container spacing={1} wrap="wrap">
        {articles.map((elem) => (
          <Grid item key={elem.id} xs={12} sm={6} md={4} lg={3} xl={2}>
            <LatestArticle
              author={elem.author}
              description={elem.description}
              placePublication={elem.placePublication}
              publishedAt={elem.publishedAt}
              title={elem.title}
              authorImg={elem.authorImg}
              imageUrl={elem.imageUrl}
            />
          </Grid>
        ))}
      </Grid>
    </Stack>
  );
};

export default LatestNews;

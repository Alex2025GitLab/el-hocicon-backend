"use client";
import {
  Box,
  Button,
  Card,
  Container,
  Grid,
  Hidden,
  Stack,
  Typography,
  useMediaQuery,
} from "@mui/material";
import React from "react";
import { useTheme } from "@mui/material";
import useMeasure from "react-use-measure";
import Title from "../ui/Title";
import LaunchButton from "../ui/buttons/LaunchButton";
import { Apple, Google } from "@mui/icons-material";
import Link from "next/link";

const Sections1 = () => {
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("md"));

  const [ref, { height }] = useMeasure();

  return (
    <Box sx={{ width: "100%" }}>
      {/* Content */}
      <Card
        variant="outlined"
        sx={{ backgroundColor: "#f5f5f5", paddingY: 5, borderRadius: 4 }}
      >
        <Stack spacing={3} alignItems="center">
          <Typography variant="caption" sx={{ letterSpacing: 4 }}>
            BIENVENIDO A EL HOCICÓN
          </Typography>
          <Box display="flex" justifyContent="center" alignItems="center">
            <Box
              sx={{
                width: "70%",
                "@media(max-width: 600px)": { width: "95%" },
              }}
            >
              <Typography variant="h4" sx={{ textAlign: "center" }}>
                Construye historias ✍️ que brinden&nbsp;
                <span style={{ color: theme.palette.primary.main }}>
                  inspiración
                </span>
                💡,&nbsp;
                <span style={{ color: theme.palette.primary.main }}>
                  conocimiento
                </span>
                📕 &nbsp;y&nbsp;
                <span style={{ color: theme.palette.primary.main }}>
                  entretenimiento🎬
                </span>
              </Typography>
            </Box>
          </Box>
          <Link href="/news">
            <Button variant="contained">Buscar noticias</Button>
          </Link>
        </Stack>
      </Card>
    </Box>
  );
};

const CustomButton = ({ children, ...props }: any) => (
  <Button
    variant="outlined"
    sx={{
      borderRadius: 4,
      color: "text.primary",
      borderColor: "text.primary",
      height: 58,
      px: 2,
    }}
    {...props}
  >
    {children}
  </Button>
);

export default Sections1;

"use client";
import { Article } from "@/types/news.interface";
import {
  Box,
  Card,
  Stack,
  Avatar,
  Typography,
  Divider,
  CardMedia,
} from "@mui/material";
import React from "react";
import FallbackImage from "../ui/FallbackImage";
import Image from "next/image";
import { deepOrange } from "@mui/material/colors";
import { formatDate } from "@/utils/utils";
import { useTheme } from "@mui/material/styles";
import { API_PUBLIC } from "@/types/strings";

const LatestArticle = ({
  title,
  imageUrl,
  publishedAt,
  placePublication,
  author,
  description,
  authorImg,
}: Article) => {
  const theme = useTheme();
  return (
    <Stack gap={2}>
      <Card>
        {imageUrl && imageUrl.length > 0 ? (
          <Image
            width={200}
            height={200}
            src={`${API_PUBLIC}/${imageUrl}`}
            style={{ objectFit: "cover", width: "100%", height: "100%" }}
            alt={
              title.length > 5
                ? `image_${title.substring(0, 5)}`
                : "image for article"
            }
          />
        ) : (
          <Box sx={{ width: "100%", height: 200 }}>
            <FallbackImage />
          </Box>
        )}
      </Card>

      <Stack direction="column" sx={{ marginTop: 2 }} gap={1}>
        <Stack direction="row">
          <Stack direction="row" gap={1}>
            {authorImg && authorImg.length > 0 ? (
              <Avatar
                sx={{ width: 24, height: 24 }}
                alt={`author_${author}`}
                src={`${API_PUBLIC}/${authorImg}`}
              />
            ) : (
              <Avatar sx={{ bgcolor: deepOrange[800], width: 24, height: 24 }}>
                H
              </Avatar>
            )}
            <Typography variant="caption">{author}</Typography>
          </Stack>
        </Stack>

        <Typography
          variant="h5"
          sx={{ fontFamily: "Merriweather", fontWeight: 700 }}
        >
          {title}
        </Typography>

        <Typography
          variant="caption"
          sx={{ color: theme.palette.text.secondary }}
        >
          {description}
        </Typography>

        {/* Place publication */}
        <Stack direction="row" gap={1}>
          <Typography
            variant="caption"
            sx={{ color: theme.palette.primary.main, fontSize: 12 }}
          >
            {placePublication}
          </Typography>
          <Divider orientation="vertical" flexItem />
          <Typography sx={{ fontSize: 12 }} variant="caption">
            {publishedAt.length > 0 ? formatDate(publishedAt) : "0/0/1900"}
          </Typography>
        </Stack>
      </Stack>
    </Stack>
  );
};

export default LatestArticle;

"use client";
import React, { ButtonHTMLAttributes } from "react";
import {
  AppBar,
  Box,
  Button,
  Container,
  IconButton,
  InputBase,
  Paper,
  Stack,
  Typography,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import useScrollPosition from "@/hooks/useScrollPosition";

import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import CallMadeIcon from "@mui/icons-material/CallMade";
import LanguageIcon from "@mui/icons-material/Language";
import MenuIcon from "@mui/icons-material/Menu";
import LinkButton from "../ui/buttons/LinkButton";
import { NewspaperOutlined, Search } from "@mui/icons-material";
import Link from "next/link";

const NAVBAR_HEIGHT = 72;

const LaunchButton = ({ sx = {}, ...props }) => {
  return (
    <Link href="/login">
      <Button variant="contained" sx={{ borderRadius: 4, ...sx }} {...props}>
        Ingresar
        <KeyboardArrowRightIcon />
      </Button>
    </Link>
  );
};

const Navbar = ({ isMain = false }: { isMain?: boolean }) => {
  const scrollPosition = useScrollPosition();
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("md"));

  return (
    <AppBar
      elevation={0}
      sx={{
        display: "grid",
        placeItems: "center",
        py: 1,
        height: NAVBAR_HEIGHT,
        borderBottom: 2,
        borderColor: scrollPosition < 10 ? "#E8ECF3" : "transparent",
        bgcolor: (scrollPosition > 10
          ? "rgba(220,220,220,.5)"
          : "transparent") as string,
        backdropFilter: (scrollPosition > 10 && "blur(5px)") as string,
        transition: "background-color 0.5s",
      }}
    >
      <Container
        sx={{
          [theme.breakpoints.up("lg")]: {
            maxWidth: "1380px!important",
          },
        }}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          flexWrap="wrap"
        >
          <Stack
            sx={{ color: theme.palette.primary.main }}
            direction="row"
            alignItems="center"
            spacing={1}
          >
            <NewspaperOutlined />
            <Typography
              variant="h4"
              sx={{ fontFamily: "Merriweather", fontWeight: 700 }}
            >
              El&nbsp;Hocicón
            </Typography>
          </Stack>

          {!isMobile && isMain && (
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="center"
              spacing={6}
              sx={{ flex: 1 }}
              flexWrap="wrap"
            >
              <Link href="/news">
                <LinkButton>
                  <Typography variant="body2">Ver Noticias</Typography>
                  <CallMadeIcon sx={{ fontSize: 12 }} />
                </LinkButton>
              </Link>

              <LinkButton>
                <Typography variant="body2">Creadores</Typography>
              </LinkButton>

              <Paper
                component="form"
                variant="outlined"
                sx={{
                  p: "1px 2px",
                  display: "flex",
                  alignItems: "center",
                  width: 250,
                  borderRadius: 3,
                  borderColor: (scrollPosition < 10
                    ? theme.palette.primary.main
                    : "#E8ECF3") as string,
                }}
              >
                <InputBase
                  sx={{ ml: 1, flex: 1, fontSize: 15 }}
                  placeholder="Buscar Noticia"
                  inputProps={{ "aria-label": "buscar noticia" }}
                />
                <IconButton type="button" sx={{ p: "3px" }} aria-label="search">
                  <Search />
                </IconButton>
              </Paper>
            </Stack>
          )}
          {/* Action Buttons */}

          <Box>
            {isMobile ? (
              <IconButton>
                <MenuIcon sx={{ color: "text.secondary" }} />
              </IconButton>
            ) : (
              <Stack direction="row" spacing={5} alignItems="center">
                <LaunchButton sx={{ borderRadius: 3 }} />
              </Stack>
            )}
          </Box>
        </Stack>
      </Container>
    </AppBar>
  );
};

export default Navbar;

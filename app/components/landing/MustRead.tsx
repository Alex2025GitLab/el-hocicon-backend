"use client";
import {
  Avatar,
  Box,
  Card,
  Divider,
  Grid,
  Hidden,
  Stack,
  Typography,
} from "@mui/material";
import React from "react";
import LatestArticle from "./LatestArticle";
import Image from "next/image";
import { Article } from "@/types/news.interface";
import deepOrange from "@mui/material/colors/deepOrange";
import { useTheme } from "@mui/material/styles";
import { formatDate } from "@/utils/utils";
import { v4 as uuidv4 } from "uuid";
import FallbackImage from "../ui/FallbackImage";

const articles: Article[] = [
  {
    id: uuidv4(),
    author: "Cody Corrall",
    description:
      "In an era of CES where companies are all-in on the AI hype machine, there are bound to be gadgets and claims that are a little odd.",
    placePublication: "TechCrunch",
    publishedAt: "2024-01-12T23:01:25Z",
    title: "CES 2024: The weirdest tech, gadgets and AI claims from Las Vegas",
    authorImg: "https://mdbcdn.b-cdn.net/img/new/avatars/1.webp",
    imageUrl:
      "https://techcrunch.com/wp-content/uploads/2024/01/creator-economy.jpg?w=1390&crop=1",
  },
  {
    id: uuidv4(),
    author: "Justin Baragona",
    description:
      "The pillow magnate claims Fox News “canceled” him for hiring Lou Dobbs, but a Fox News source with knowledge told The Daily Beast that Lindell simply hasn’t paid his bills.",
    placePublication: "Daily Beast",
    publishedAt: "2024-01-12T22:31:47Z",
    title: "Fox News Kicks Mike Lindell and His MyPillow Ads to the Curb",
    authorImg: "https://mdbcdn.b-cdn.net/img/new/avatars/2.webp",
    imageUrl:
      "https://img.thedailybeast.com/image/upload/c_crop,d_placeholder_euli9k,h_1743,w_3100,x_0,y_129/dpr_2.0/c_limit,w_740/fl_lossy,q_auto/v1705090988/GettyImages-1246708664_gqbskj",
  },
  {
    id: uuidv4(),
    author: "Nina Golgowski",
    description:
      "Several children became infected with the virus after a sick child was taken to a day care despite orders to quarantine.",
    placePublication: "HuffPost",
    publishedAt: "2024-01-12T22:30:59Z",
    title:
      "Philadelphia Warns Of Measles Outbreak After Quarantined Kid Sent To Day Care Anyway",
    authorImg: "https://mdbcdn.b-cdn.net/img/new/avatars/3.webp",
    imageUrl:
      "https://img.huffingtonpost.com/asset/65a191132300003100806deb.jpeg?cache=j2CFTZleCk&ops=1200_630",
  },
  {
    id: uuidv4(),
    author: "Kate Gibson",
    description:
      'Starbucks is appealing a ruling ordering the coffee chain to reinstate terminated workers known as the "Memphis Seven."',
    placePublication: "CBS News",
    publishedAt: "2024-01-12T22:28:00Z",
    title:
      "Supreme Court to hear case on Starbucks' firing of pro-union baristas",
    authorImg: "https://mdbcdn.b-cdn.net/img/new/avatars/4.webp",
    imageUrl:
      "https://assets2.cbsnewsstatic.com/hub/i/r/2022/06/07/17015c2b-d48b-4412-b480-ee725be672af/thumbnail/1200x630/55458fc2fd161ade37e75169c8ae4c73/download.jpg?v=50926e3bde2e7c9caafa13eb3f9693b5",
  },
];

// 4 articulos aleatorios
const MustRead = () => {
  const theme = useTheme();
  return (
    <Hidden mdDown>
      <Stack gap={3}>
        <Typography variant="h4" sx={{ fontWeight: 700 }}>
          Te podría interesar...
        </Typography>

        <Grid container gap={3} wrap="nowrap">
          <Grid item xs={3}>
            <Stack gap={2}>
              <Card>
                {articles[0].imageUrl && articles[0].imageUrl.length > 0 ? (
                  <Image
                    width={200}
                    height={200}
                    src={articles[0].imageUrl}
                    style={{
                      objectFit: "cover",
                      width: "100%",
                      height: "100%",
                    }}
                    alt={
                      articles[0].title.length > 5
                        ? `image_${articles[0].title.substring(0, 5)}`
                        : "image for article"
                    }
                  />
                ) : (
                  <Box sx={{ width: "100%", height: 200 }}>
                    <FallbackImage />
                  </Box>
                )}
              </Card>

              <Stack direction="column" sx={{ marginTop: 2 }} gap={1}>
                <Stack direction="row">
                  <Stack direction="row" gap={1}>
                    {articles[0].authorImg ? (
                      <Avatar
                        sx={{ width: 24, height: 24 }}
                        alt={`author_${articles[0].author}`}
                        src={articles[0].authorImg}
                      />
                    ) : (
                      <Avatar
                        sx={{ bgcolor: deepOrange[800], width: 24, height: 24 }}
                      >
                        H
                      </Avatar>
                    )}
                    <Typography variant="caption">
                      {articles[0].author}
                    </Typography>
                  </Stack>
                </Stack>

                <Typography
                  variant="h5"
                  sx={{ fontFamily: "Merriweather", fontWeight: 700 }}
                >
                  {articles[0].title}
                </Typography>

                <Typography
                  variant="caption"
                  sx={{ color: theme.palette.text.secondary }}
                >
                  {articles[0].description}
                </Typography>

                {/* Place publication */}
                <Stack direction="row" gap={1}>
                  <Typography
                    variant="caption"
                    sx={{ color: theme.palette.primary.main, fontSize: 12 }}
                  >
                    {articles[0].placePublication}
                  </Typography>
                  <Divider orientation="vertical" flexItem />
                  <Typography sx={{ fontSize: 12 }} variant="caption">
                    {articles[0].publishedAt.length > 0
                      ? formatDate(articles[0].publishedAt)
                      : "0/0/1900"}
                  </Typography>
                </Stack>
              </Stack>
            </Stack>
          </Grid>

          <Grid item xs={6}>
            <Card
              variant="elevation"
              sx={{ borderRadius: 2, position: "relative", margin: 0 }}
            >
              <Image
                style={{ width: "100%", objectFit: "cover" }}
                src={`${articles[1].imageUrl}`}
                width={400}
                height={400}
                alt="imagess"
              />
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "end",
                  overflow: "hidden",
                  position: "absolute",
                  bottom: 0,
                  color: "white",
                  zIndex: 20,
                  width: "100%",
                  paddingBottom: 2,
                  height: "100%",
                  backgroundImage:
                    "linear-gradient(to bottom, transparent, rgba(0, 0, 0, 0.8))",
                  paddingX: 2,
                }}
              >
                <Stack direction="column" sx={{ marginTop: 1 }} gap={1}>
                  <Stack direction="row">
                    <Stack direction="row" gap={1}>
                      {articles[1].authorImg ? (
                        <Avatar
                          sx={{ width: 24, height: 24 }}
                          alt={`author_${articles[1].author}`}
                          src={articles[1].authorImg}
                        />
                      ) : (
                        <Avatar
                          sx={{
                            bgcolor: deepOrange[800],
                            width: 24,
                            height: 24,
                          }}
                        >
                          H
                        </Avatar>
                      )}
                      <Typography variant="caption">
                        {articles[1].author}
                      </Typography>
                    </Stack>
                  </Stack>

                  <Typography
                    variant="h5"
                    sx={{ fontFamily: "Merriweather", fontWeight: 700 }}
                  >
                    {articles[1].title}
                  </Typography>

                  <Typography variant="caption">
                    {articles[1].description}
                  </Typography>

                  {/* Place publication */}
                  <Stack direction="row" gap={1}>
                    <Typography
                      variant="caption"
                      sx={{ color: theme.palette.primary.main, fontSize: 12 }}
                    >
                      {articles[1].placePublication}
                    </Typography>
                    <Typography sx={{ fontSize: 12 }} variant="caption">
                      {articles[1].publishedAt.length > 0
                        ? formatDate(articles[1].publishedAt)
                        : "0/0/1900"}
                    </Typography>
                  </Stack>
                </Stack>
              </Box>
            </Card>
          </Grid>

          <Grid item xs={3}>
            <Stack gap={1}>
              <SimpleArticle
                author={articles[2].author}
                description={articles[2].description}
                placePublication={articles[2].placePublication}
                publishedAt={articles[2].publishedAt}
                title={articles[2].title}
                authorImg={articles[2].authorImg}
                imageUrl={articles[2].imageUrl}
              />

              <SimpleArticle
                author={articles[3].author}
                description={articles[3].description}
                placePublication={articles[3].placePublication}
                publishedAt={articles[3].publishedAt}
                title={articles[3].title}
                authorImg={articles[3].authorImg}
                imageUrl={articles[3].imageUrl}
              />
            </Stack>
          </Grid>
        </Grid>
      </Stack>
    </Hidden>
  );
};

export default MustRead;

const SimpleArticle = ({
  title,
  publishedAt,
  author,
  placePublication,
  imageUrl,
  authorImg,
}: Article) => {
  const theme = useTheme();
  return (
    <Stack direction="column" gap={1}>
      <Box sx={{ borderRadius: 2, overflow: "hidden", maxHeight: 100 }}>
        <Image
          src={imageUrl as string}
          style={{ width: "100%", objectFit: "cover" }}
          alt="fafasd"
          width={300}
          height={200}
        />
      </Box>

      <Stack direction="row" gap={1}>
        {authorImg ? (
          <Avatar
            sx={{ width: 24, height: 24 }}
            alt={`author_${author}`}
            src={authorImg}
          />
        ) : (
          <Avatar sx={{ bgcolor: deepOrange[800], width: 24, height: 24 }}>
            H
          </Avatar>
        )}
        <Typography variant="caption">{author}</Typography>
      </Stack>

      <Typography
        variant="h6"
        sx={{
          fontFamily: "Merriweather",
          fontWeight: 700,
          overflow: "hidden",
          whiteSpace: "nowrap",
          textOverflow: "ellipsis",
        }}
      >
        {title}
      </Typography>

      {/* Place publication */}
      <Stack direction="row" gap={1}>
        <Typography
          variant="caption"
          sx={{ color: theme.palette.primary.main, fontSize: 12 }}
        >
          {placePublication}
        </Typography>
        <Divider orientation="vertical" flexItem />
        <Typography sx={{ fontSize: 12 }} variant="caption">
          {publishedAt.length > 0 ? formatDate(publishedAt) : "0/0/1900"}
        </Typography>
      </Stack>
    </Stack>
  );
};

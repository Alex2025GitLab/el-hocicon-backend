"use client";
import {
  Box,
  Button,
  Card,
  Hidden,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { useTheme } from "@mui/material/styles";

const Subscribe = () => {
  const theme = useTheme();
  return (
    <Hidden mdDown>
      <Box sx={{ width: "100%" }}>
        {/* Content */}
        <Card
          variant="outlined"
          sx={{ backgroundColor: "#f5f5f5", paddingY: 5, borderRadius: 4 }}
        >
          <Stack
            direction="row"
            justifyContent="space-evenly"
            sx={{ marginX: 5 }}
          >
            <Stack spacing={3} alignItems="left">
              <Typography variant="caption" sx={{ letterSpacing: 4 }}>
                NO TE PIERDAS NADA
              </Typography>
              <Box display="flex" justifyContent="start" alignItems="center">
                <Box sx={{ width: "70%" }}>
                  <Typography variant="h5">
                    Obtén las últimas noticias al &nbsp;
                    <span style={{ color: theme.palette.primary.main }}>
                      suscribirte
                    </span>
                    ✍️,&nbsp;
                  </Typography>
                </Box>
              </Box>
            </Stack>

            <Stack direction="row" alignItems="center" spacing={1}>
              <TextField sx={{ minHeight: "48px" }} label="Tu correo" />
              <Button sx={{ minHeight: "48px" }} variant="contained">
                Suscríbete
              </Button>
            </Stack>
          </Stack>
        </Card>
      </Box>
    </Hidden>
  );
};

export default Subscribe;

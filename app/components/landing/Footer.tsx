"use client";
import {
  Box,
  Divider,
  Container,
  Grid,
  Stack,
  Typography,
  IconButton,
} from "@mui/material";
import React from "react";
import Title from "../ui/Title";
import {
  Book,
  Reddit,
  Twitter,
  Chat,
  Telegram,
  GitHub,
} from "@mui/icons-material";
import OutlinedButton from "../ui/buttons/OutlinedButton";

const footerContent = {
  protocols: {
    title: "Negocios",
    links: [{ title: "PYME's" }, { title: "Empleos" }, { title: "Mercado" }],
  },
  governance: {
    title: "Tecnología",
    links: [
      { title: "Innovación" },
      { title: "Inteligencia artificial" },
      { title: "Futuro" },
      { title: "Domótica" },
    ],
  },
  support: {
    title: "Viajes",
    links: [
      { title: "Destinos" },
      { title: "Comida y bebida" },
      { title: "Noticias", subtitle: "Videos" },
      { title: "Hoteles" },
    ],
  },
  developers: {
    title: "Entretenimiento",
    links: [
      { title: "Películas" },
      { title: "Artistas" },
      { title: "Television" },
      { title: "Influencer" },
      { title: "Viral" },
    ],
  },
  subscribe: {
    title: "Suscribete a El Hocicón",
    subtitle: "Obtén las últimas noticias",
  },
  socials: [
    { icon: Book },
    { icon: Reddit },
    { icon: Twitter },
    { icon: Chat },
    { icon: Telegram },
    { icon: GitHub },
  ],
  copyright: {
    left: "© 2024",
    center: "Alexander Nina Pacajes",
    right: "aaalex2025@gmail.com",
  },
};

const LinkSection = ({
  title,
  links,
}: {
  title: string;
  links: { title: string }[];
}) => (
  <Stack spacing={2.5}>
    <Title>{title}</Title>

    {links.map(({ title }) => (
      <Typography
        key={title}
        variant="body2"
        color="text.secondary"
        sx={{
          cursor: "pointer",
          "&:hover": {
            color: "text.primary",
          },
        }}
      >
        {title}
      </Typography>
    ))}
  </Stack>
);

const Footer = () => {
  return (
    <Box>
      <Divider sx={{ mb: 10 }} />
      <Container>
        <Grid container spacing={8} flexWrap="wrap-reverse">
          {/* Links */}
          <Grid item xs={12} md={6} lg={7} xl={8}>
            <Grid container spacing={2}>
              {/* Protocols */}
              <Grid item xs={6} sm={3} md={6} lg={3}>
                <LinkSection {...footerContent.protocols} />
              </Grid>

              {/* Governance */}
              <Grid item xs={6} sm={3} md={6} lg={3}>
                <LinkSection {...footerContent.governance} />
              </Grid>

              {/* Support */}
              <Grid item xs={6} sm={3} md={6} lg={3}>
                <LinkSection {...footerContent.support} />
              </Grid>

              {/* Developers */}
              <Grid item xs={6} sm={3} md={6} lg={3}>
                <LinkSection {...footerContent.developers} />
              </Grid>
            </Grid>
          </Grid>

          {/* Subscribe */}
          <Grid item xs={12} md={6} lg={5} xl={4}>
            <Stack>
              <Title sx={{ mb: 1 }}>{footerContent.subscribe.title}</Title>

              <Typography variant="body2" color="text.secondary">
                {footerContent.subscribe.subtitle}
              </Typography>

              <OutlinedButton arrow sx={{ height: 60, my: 3 }}>
                Subscríbete
              </OutlinedButton>

              <Stack
                direction="row"
                spacing={1}
                alignItems="center"
                justifyContent="space-between"
                flexWrap="wrap"
              >
                {footerContent.socials.map((item, i) => (
                  <IconButton key={i}>
                    <item.icon />
                  </IconButton>
                ))}
              </Stack>
            </Stack>
          </Grid>
        </Grid>

        <Divider sx={{ mt: 6, mb: 5 }} />

        <Stack
          direction={{ xs: "column", md: "row" }}
          justifyContent="space-between"
          alignItems="center"
          spacing={1}
          sx={{ pb: 6 }}
        >
          <Typography variant="body2" color="text.secondary">
            {footerContent.copyright.left}
          </Typography>

          <Typography variant="body2" color="text.secondary">
            {footerContent.copyright.center}
          </Typography>

          <Typography variant="body2" color="text.secondary">
            {footerContent.copyright.right}
          </Typography>
        </Stack>
      </Container>
    </Box>
  );
};

export default Footer;

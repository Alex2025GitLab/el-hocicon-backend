import { API_LOGIN, SERVER_ERROR, TOKEN } from "@/types/strings";
import axios from "axios";
import { serialize } from "cookie";
import { NextRequest, NextResponse } from "next/server";

export async function POST(request: Request) {
  try {
    const body = await request.json();
    const serverResponse = await axios.post(API_LOGIN, body);

    if (serverResponse.data.status === 200) {
      const serialized = serialize(TOKEN, serverResponse.data.access_token, {
        httpOnly: true,
        secure: process.env.NODE_ENV === "production",
        maxAge: 2 * 60 * 60,
        path: "/",
      });
      const responseHeaders = NextResponse.json(
        { message: "Ingreso con éxito" },
        {
          status: 200,
        }
      );
      responseHeaders.headers.set("Set-Cookie", serialized);
      return responseHeaders;
    } else {
      return NextResponse.json(serverResponse.data);
    }
  } catch (error) {
    return NextResponse.json(SERVER_ERROR, { status: 500 });
  }
}

// Logout
export async function GET(request: NextRequest) {
  try {
    const admin_token: any = request.cookies.get(TOKEN);

    if (!admin_token) {
      return NextResponse.json(
        { error: "No iniciaste sesión!" },
        { status: 401 }
      );
    }

    const serialized = serialize(TOKEN, "", {
      httpOnly: true,
      secure: process.env.NODE_ENV === "production",
      maxAge: -1,
      path: "/",
    });

    const responseHeaders = NextResponse.json(
      { message: "Sesión cerrada" },
      {
        status: 200,
      }
    );
    responseHeaders.headers.set("Set-Cookie", serialized);

    return responseHeaders;
  } catch (error) {
    return NextResponse.json(SERVER_ERROR, { status: 400 });
  }
}

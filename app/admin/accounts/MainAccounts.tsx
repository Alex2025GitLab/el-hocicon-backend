"use client";

import { User } from "@/types/user.interface";
import { Column, convertirFormatoDashboard, formatDate } from "@/utils/utils";
import {
  Avatar,
  Box,
  Container,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import Image from "next/image";
import React from "react";
import { API_PUBLIC } from "@/types/strings";

interface MainAccounts {
  data: User[];
  columns: Column[];
}
const MainAccounts = ({ data, columns }: MainAccounts) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      // @ts-ignore
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.isImage ? (
                            <Avatar
                              src={`${API_PUBLIC}/${value}`}
                              sx={{
                                margin: "8px 0 8px 8px !important",
                                cursor: "pointer",
                              }}
                              alt={`profile_${value}`}
                              aria-controls="menu-list-grow"
                              aria-haspopup="true"
                              color="inherit"
                            />
                          ) : (
                            // <Box sx={{ }}>
                            //   <Image
                            //     src={`${API_PUBLIC}/${value}`}
                            //     alt={`profile_${value}`}
                            //     width={30}
                            //     height={30}
                            //   />
                            // </Box>
                            <Typography variant="body2">
                              {column.isDate
                                ? convertirFormatoDashboard(value)
                                : value}
                            </Typography>
                          )}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default MainAccounts;

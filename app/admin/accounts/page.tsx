import {
  Container,
  Paper,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
} from "@mui/material";
import React from "react";
import MainAccounts from "./MainAccounts";
import { Column } from "@/utils/utils";
import { User } from "@/types/user.interface";
import { API_USER } from "@/types/strings";

const columns: Column[] = [
  { id: "username", label: "Usuario", minWidth: 150 },
  { id: "email", label: "Correo", minWidth: 150 },
  {
    id: "profile_img",
    label: "Foto",
    minWidth: 150,
    isImage: true,
  },
  {
    id: "created_at",
    label: "Fecha creación",
    minWidth: 50,
    isDate: true,
    align: "right",
  },
];

const getAccountsData = async (): Promise<User[]> => {
  const res = await fetch(API_USER, { next: { revalidate: 0 } });
  if (!res.ok) {
    return res.text().then((text) => {
      throw new Error(text);
    });
  }
  const data = await res.json();
  return data;
};
const Accounts = async () => {
  const dataUsers: User[] = await getAccountsData();
  return (
    <Container>
      <Stack gap={2}>
        <Stack direction="row" justifyContent="start">
          <Typography>Cuentas creadas</Typography>
        </Stack>

        <MainAccounts data={dataUsers} columns={columns} />
      </Stack>
    </Container>
  );
};

export default Accounts;

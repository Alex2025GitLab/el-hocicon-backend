"use client";
import React, { useState } from "react";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import { SubmitHandler, useForm } from "react-hook-form";
import { Column, convertirFormatoDashboard } from "@/utils/utils";
import { NewsDashboard } from "@/types/news.interface";
import {
  Avatar,
  Backdrop,
  Box,
  Button,
  Card,
  Container,
  Fade,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Modal,
  Paper,
  Select,
  Stack,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import { AddOutlined, Close, Delete, Edit } from "@mui/icons-material";
import { MDXProvider } from "@mdx-js/react";
import axios from "axios";
import {
  API_NEWS,
  API_PUBLIC,
  ERROR,
  SERVER_ERROR,
  SUCCESS,
} from "@/types/strings";
import { toast } from "sonner";
import { User, UserNews } from "@/types/user.interface";

import Image from "next/image";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 900,
  bgcolor: "background.paper",
  borderRadius: 2,
  boxShadow: 24,
};

const NewArticleSchema = z.object({
  title: z.string().min(1, "El titulo es requerido"),
  placePublication: z.string(),
  description: z
    .string()
    .min(10, { message: "Debe tener al menos 10 caracteres" }),
  content: z.string().min(10, { message: "Debe tener al menos 10 caracteres" }),
  autor: z.number(),
});

type FormInput = z.infer<typeof NewArticleSchema>;

interface MainPage {
  dataNews: NewsDashboard[];
  dataUsers: UserNews[];
  columns: Column[];
}

const MainPage = ({ dataNews, dataUsers, columns }: MainPage) => {
  const [dynamicNews, setDynamicNews] = useState<NewsDashboard[]>(dataNews);

  const {
    register,
    handleSubmit,
    setValue,
    getValues,
    formState: { errors },
  } = useForm<FormInput>({
    resolver: zodResolver(NewArticleSchema),
    defaultValues: {
      content: "",
      description: "",
      title: "",
      placePublication: "",
      autor: -1,
    },
  });

  const [image, setImage] = useState<File | string | undefined>(undefined);
  const [page, setPage] = React.useState(0);

  const [mdxData, setMDXData] = React.useState("");
  const [clickSubmit, setClickSubmit] = useState<boolean>(true);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [openDelete, setOpenDelete] = useState(false);
  const handleDeleteOpen = () => setOpen(true);
  const handleDeleteClose = () => setOpen(false);

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleDeleteNew = async (id: number) => {
    console.log(id);
    toast("Eliminando Noticia...");
    await axios
      .delete(`${API_NEWS}/${id}`)
      .then(async function (res) {
        console.log(res);
        if (res.data.success) {
          toast.success(SUCCESS, { description: res.data.success });
        } else {
          toast.error(ERROR, {
            description: res.data.response,
          });
          setClickSubmit(true);
        }
      })
      .catch(function (error) {
        toast.error(ERROR, {
          description: SERVER_ERROR,
        });
      });
  };

  const handleUpdateNew = async (id: number) => {
    console.log(id);
    const findNew = dataNews.find((elem) => elem.id === id);

    if (findNew) {
      setValue("content", findNew?.content);
      setValue("description", findNew?.description);
      setValue("placePublication", findNew?.place_publication);
      setValue("title", findNew?.title);
    }
    setOpen(true);
  };

  const newArtile: SubmitHandler<FormInput> = async (data) => {
    const dataSend = new FormData();
    dataSend.set("title", getValues("title"));
    dataSend.set("content", getValues("content"));
    // @ts-ignore
    dataSend.set("idUser", getValues("autor") as number);
    dataSend.set("description", getValues("description"));
    dataSend.set("placePublication", getValues("placePublication"));
    if (image !== undefined) {
      dataSend.set("file", image);
    }
    console.log(dataSend);
    if (clickSubmit) {
      setClickSubmit(false);
      await axios
        .post(`${API_NEWS}`, dataSend)
        .then(async function (res) {
          console.log(res);
          if (res.data.success) {
            toast.success(SUCCESS, { description: res.data.success });
            const updateTable = await axios.get(`${API_NEWS}`);
            // if (updateTable.data.news) {
            //   setDynamicNews(updateTable.data.news);
            // }
            handleClose();
          } else {
            toast.error(ERROR, {
              description: res.data.response,
            });
            setClickSubmit(true);
          }
        })
        .catch(function (error) {
          toast.error(ERROR, {
            description: SERVER_ERROR,
          });
          setClickSubmit(true);
        });
    }
  };

  const handleUpdateNews = async () => {
    // TODO actualizar noticias
  };

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Stack
              direction="row"
              justifyContent="space-between"
              alignItems="center"
              sx={{
                borderBottom: 1,
                paddingY: 1,
                paddingX: 2,
                borderColor: "#E50A14",
              }}
            >
              <Typography variant="h5">Nueva Noticia</Typography>
              <IconButton>
                <Close />
              </IconButton>
            </Stack>

            {/* Body */}
            <form onSubmit={handleSubmit(newArtile)}>
              <Box sx={{ width: "100%" }}>
                <Container sx={{ paddingY: 2 }}>
                  <Stack gap={2}>
                    <Grid container gap={1} wrap="nowrap">
                      <Grid item xs={12}>
                        <FormControl fullWidth>
                          <InputLabel id="demo-simple-select-label">
                            Autor / Editor
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-label"
                            label="Autor"
                            required
                            {...register("autor")}
                          >
                            {dataUsers.map((elem) => (
                              <MenuItem key={elem.id} value={elem.id}>
                                <Stack
                                  direction="row"
                                  gap={2}
                                  alignItems="center"
                                >
                                  <Avatar
                                    src={
                                      elem.profile_img.length > 0
                                        ? `${API_PUBLIC}/${elem.profile_img}`
                                        : ""
                                    }
                                    sx={{
                                      margin: "0px 0 0px 0px !important",
                                      cursor: "pointer",
                                    }}
                                    alt={`profile_${elem.username}`}
                                    aria-controls="menu-list-grow"
                                    aria-haspopup="true"
                                    color="inherit"
                                  />
                                  <Typography>{elem.username}</Typography>
                                </Stack>
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Grid>
                    </Grid>
                    <Grid container gap={1} wrap="nowrap">
                      <Grid item xs={6}>
                        <TextField
                          required
                          fullWidth
                          autoComplete="off"
                          id="outlined-basic"
                          label="Titulo"
                          variant="outlined"
                          {...register("title")}
                        />
                      </Grid>
                      <Grid item xs={6}>
                        <TextField
                          autoComplete="off"
                          fullWidth
                          id="outlined-basic"
                          label="Lugar de publicación"
                          variant="outlined"
                          {...register("placePublication")}
                        />
                      </Grid>
                    </Grid>

                    <Grid container gap={1} wrap="nowrap" alignItems="end">
                      <Grid item xs={6}>
                        <Stack gap={1}>
                          <Typography variant="body2">
                            Imagen de la noticia
                          </Typography>
                          <TextField
                            type="file"
                            fullWidth
                            inputProps={{
                              accept: ".jpg, .jpeg",
                            }}
                            onChange={(
                              e: React.ChangeEvent<HTMLInputElement>
                            ) => {
                              setImage(e.target.files?.[0]);
                            }}
                            id="outlined-basic"
                            variant="outlined"
                          />
                        </Stack>
                      </Grid>
                      <Grid item xs={6}>
                        <TextField
                          required
                          fullWidth
                          autoComplete="off"
                          id="outlined-multiline-flexible"
                          label="Breve descripción"
                          multiline
                          maxRows={4}
                          {...register("description")}
                        />
                      </Grid>
                    </Grid>
                    <Grid container>
                      <Grid item xs={12}>
                        <TextField
                          label="Contenido de la noticia"
                          multiline
                          autoComplete="off"
                          fullWidth
                          rows={5}
                          variant="outlined"
                          {...register("content")}
                        />
                      </Grid>
                    </Grid>
                  </Stack>
                </Container>
              </Box>
              <Stack
                direction="row"
                justifyContent="end"
                alignItems="center"
                gap={2}
                sx={{
                  borderTop: 1,
                  paddingY: 1,
                  paddingX: 2,
                  borderColor: "#E50A14",
                }}
              >
                <Button variant="outlined" onClick={() => setOpen(false)}>
                  Cancelar
                </Button>
                <Button variant="contained" type="submit">
                  Guardar
                </Button>
              </Stack>
            </form>
          </Box>
        </Fade>
      </Modal>

      <Container>
        <Stack gap={2}>
          <Stack direction="row" justifyContent="space-between">
            <Typography>Noticias</Typography>
            <Button
              variant="contained"
              startIcon={<AddOutlined />}
              onClick={handleOpen}
            >
              Añadir Noticia
            </Button>
          </Stack>

          <Paper sx={{ width: "100%", overflow: "hidden" }}>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map((column: Column) => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {dynamicNews
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.id}
                        >
                          {columns.map((column: Column) => {
                            //@ts-ignore
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {column.isImage ? (
                                  <Card>
                                    {value.length > 0 ? (
                                      <Image
                                        src={`${API_PUBLIC}/${value}`}
                                        alt={`image_${value}`}
                                        width={100}
                                        style={{
                                          width: "100%",
                                          height: "100%",
                                        }}
                                        height={50}
                                      />
                                    ) : (
                                      <Box
                                        sx={{
                                          width: "100px",
                                          height: "50px",
                                          display: "grid",
                                          borderRadius: 2,
                                          bgcolor: "gray",
                                          placeItems: "center",
                                        }}
                                      >
                                        <Typography>No Img</Typography>
                                      </Box>
                                    )}
                                  </Card>
                                ) : (
                                  // <Box sx={{ }}>
                                  //   <Image
                                  //     src={`${API_PUBLIC}/${value}`}
                                  //     alt={`profile_${value}`}
                                  //     width={30}
                                  //     height={30}
                                  //   />
                                  // </Box>
                                  <Typography variant="body2">
                                    {column.isDate
                                      ? convertirFormatoDashboard(value)
                                      : value}
                                  </Typography>
                                )}
                              </TableCell>
                            );
                          })}
                          <TableCell key="modify">
                            <Stack direction="row" gap={2} alignItems="center">
                              <IconButton
                                onClick={() => handleUpdateNew(row.id)}
                                aria-label="update"
                              >
                                <Edit />
                              </IconButton>
                              <IconButton
                                onClick={() => handleDeleteNew(row.id)}
                                aria-label="delete"
                                color="primary"
                              >
                                <Delete />
                              </IconButton>
                            </Stack>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={dynamicNews.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Paper>
        </Stack>
      </Container>
    </>
  );
};

export default MainPage;

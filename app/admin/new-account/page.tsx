"use client";
import React, { useState } from "react";
import {
  Button,
  Container,
  Grid,
  IconButton,
  Typography,
  Stack,
  TextField,
  Paper,
} from "@mui/material";
import { AddOutlined } from "@mui/icons-material";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import { SubmitHandler, useForm } from "react-hook-form";
import axios from "axios";
import { API_USER, ERROR, SERVER_ERROR, SUCCESS } from "@/types/strings";
import { toast } from "sonner";

const NewAccountSchema = z.object({
  email: z
    .string()
    .email({
      message: "Correo inválido, por favor introduzca una dirección válida",
    })
    .min(1, "El correo es requerido"),
  password: z
    .string()
    .min(8, { message: "Debe tener al menos 8 caracteres." })
    .regex(
      /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,
      "Debe contener al menos 1 (número, letra minúscula, letra mayúscula)"
    ),
  username: z.string().min(4, { message: "Debe tener al menos 4 caracteres" }),
});

type FormInput = z.infer<typeof NewAccountSchema>;
const NewAccount = () => {
  const [profile, setProfile] = useState<File | string | undefined>(undefined);
  const [clickSubmit, setClickSubmit] = useState<boolean>(true);
  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<FormInput>({
    resolver: zodResolver(NewAccountSchema),
    defaultValues: {
      email: "",
      username: "",
      password: "",
    },
  });

  const newAccount: SubmitHandler<FormInput> = async (data) => {
    const dataSend = new FormData();
    dataSend.set("username", getValues("username"));
    dataSend.set("email", getValues("email"));
    dataSend.set("password", getValues("password"));
    if (profile !== undefined) {
      dataSend.set("file", profile);
    }

    if (clickSubmit) {
      setClickSubmit(false);
      await axios
        .post(`${API_USER}`, dataSend, { withCredentials: true })
        .then(function (res) {
          console.log(res);
          if (res.data.status === 200) {
            toast.success(SUCCESS, { description: res.data.response });
          } else {
            toast.error(ERROR, {
              description: res.data.response,
            });
            setClickSubmit(true);
          }
        })
        .catch(function (error) {
          toast.error(ERROR, {
            description: SERVER_ERROR,
          });
          setClickSubmit(true);
        });
    }
  };

  return (
    <Container>
      <Stack gap={2}>
        <Stack direction="row" justifyContent="space-between">
          <Typography>Crear cuenta</Typography>
        </Stack>

        <Paper
          sx={{
            width: "100%",
            overflow: "hidden",
            marginX: 3,
            padding: 3,
            bgcolor: "white",
          }}
        >
          <form onSubmit={handleSubmit(newAccount)}>
            <Grid container gap={3}>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  label="Nombre cuenta"
                  id="username"
                  {...register("username")}
                />
              </Grid>

              <Grid item xs={12}>
                <Stack gap={1}>
                  <Typography variant="body1">
                    Foto de perfil (Solo .jpg o .jpeg)
                  </Typography>
                  <TextField
                    inputProps={{
                      accept: ".jpg, .jpeg",
                    }}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setProfile(e.target.files?.[0]);
                    }}
                    type="file"
                    id="logo1"
                  />
                </Stack>
              </Grid>

              <Grid wrap="nowrap" container gap={2}>
                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    fullWidth
                    label="Correo electrónico"
                    id="email"
                    {...register("email")}
                  />
                </Grid>

                <Grid item xs={12} md={6}>
                  <TextField
                    required
                    fullWidth
                    label="Contraseña"
                    id="password"
                    type="password"
                    {...register("password")}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Stack
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                mt: 3,
              }}
            >
              <Button
                variant="contained"
                type="submit"
                startIcon={<AddOutlined />}
              >
                Crear cuenta
              </Button>
            </Stack>
          </form>
        </Paper>
      </Stack>
    </Container>
  );
};

export default NewAccount;

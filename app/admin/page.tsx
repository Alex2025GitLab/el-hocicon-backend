import React from "react";
import { Column } from "@/utils/utils";
import MainPage from "./MainPage";
import { NewsDashboard } from "@/types/news.interface";
import { API_NEWS } from "@/types/strings";
import { User, UserNews } from "@/types/user.interface";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 900,
  bgcolor: "background.paper",
  borderRadius: 2,
  boxShadow: 24,
};

const columns: Column[] = [
  { id: "id", label: "Id", minWidth: 50, align: "right" },
  { id: "title", label: "Titulo", minWidth: 150 },
  {
    id: "image_url",
    label: "Imagen principal",
    minWidth: 100,
    isImage: true,
  },
  { id: "place_publication", label: "Lugar de publicación", minWidth: 150 },
  { id: "description", label: "Descripción", minWidth: 150 },
  { id: "content", label: "Contenido", minWidth: 150 },
  { id: "created_at", label: "Fecha creación", isDate: true, minWidth: 100 },
  {
    id: "updated_at",
    label: "Fecha actualización",
    isDate: true,
    minWidth: 100,
  },
  { id: "created_by", label: "Creado por", minWidth: 100 },
  { id: "updated_by", label: "Ultima edición", minWidth: 150 },
  // { id: "modify", label: "Modificar", minWidth: 100 },
];

interface ServerGetNews {
  news: NewsDashboard[];
  users: UserNews[];
}
const getAllDataNews = async (): Promise<ServerGetNews> => {
  const res = await fetch(API_NEWS, { next: { revalidate: 0 } });
  if (!res.ok) {
    return res.text().then((text) => {
      throw new Error(text);
    });
  }
  const data = await res.json();
  return data;
};

const AdminHome = async () => {
  const completeDataNews: ServerGetNews = await getAllDataNews();
  return (
    <MainPage
      columns={columns}
      dataNews={completeDataNews.news}
      dataUsers={completeDataNews.users}
    />
  );
};
export default AdminHome;

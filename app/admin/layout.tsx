"use client";
import React from "react";
import { useTheme } from "@mui/material/styles";
import Link from "next/link";
import {
  AppBar,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
  Box,
  styled,
  Hidden,
} from "@mui/material";

import AdminHeader from "../components/admin/AdminHeader";
import Sidebar from "../components/admin/Sidebar";
const DRAWER_WIDTH = 260;

const AdminLayout = ({ children }: { children: React.ReactNode }) => {
  const theme = useTheme();
  return (
    <div>
      <AppBar
        enableColorOnDark
        position="fixed"
        color="inherit"
        elevation={0}
        sx={{
          bgcolor: theme.palette.background.default,
        }}
      >
        <Toolbar>
          <AdminHeader />
        </Toolbar>
      </AppBar>

      <div>
        {/* drawer */}
        <Hidden mdDown>
          <Sidebar />
        </Hidden>

        {/* main content */}
        <Box
          component="main"
          sx={{
            flexGrow: 1,
            borderRadius: 2,
            bgcolor: "#FEF0F3",
            ml: `${DRAWER_WIDTH}px`,
            "@media (max-width: 900px)": { ml: 2 },
            mt: ["48px", "56px", "64px"],
            p: 3,
            mr: 2,
          }}
        >
          {children}
        </Box>
      </div>
    </div>
  );
};

export default AdminLayout;

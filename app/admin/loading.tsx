"use client";
import { CircularProgress, Container } from "@mui/material";
import React from "react";

const Loading = () => {
  return (
    <Container sx={{ display: "grid", placeItems: "center" }}>
      <CircularProgress />
    </Container>
  );
};

export default Loading;

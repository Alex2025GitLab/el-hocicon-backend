"use client";
import { Article, Filters, initialFilters } from "@/types/news.interface";
import { Box, CircularProgress, Container, Stack } from "@mui/material";
import React, { useEffect, useState } from "react";
import Navbar from "../components/landing/Navbar";
import SearchHeader from "../components/news/SearchHeader";
import SearchBody from "../components/news/SearchBody";
import Footer from "../components/landing/Footer";
import { API_LANDING, ERROR, SERVER_ERROR } from "@/types/strings";
import axios from "axios";
import { toast } from "sonner";
interface MainNews {
  latestNews: Article[];
}
const MainNews = ({ latestNews }: MainNews) => {
  const [queryNews, setQueryNews] = useState<Article[]>(latestNews);

  const [query, setQuery] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const [filter, setFilter] = useState<Filters>(initialFilters);

  useEffect(() => {
    if (query.length === 0) setIsLoading(false);
  }, [query]);

  const handleSearchInput = async () => {
    await axios
      .post(API_LANDING, { q: query })
      .then(function (res) {
        console.log(res);
        if (res.data.success) {
          setQueryNews(res.data.news);
        } else {
          toast.error(ERROR, {
            description: res.data.response,
          });
        }
      })
      .catch(function (error) {
        toast.error(ERROR, {
          description: SERVER_ERROR,
        });
      });
  };
  return (
    <Container>
      <Navbar />

      <Stack gap={3} sx={{ mt: 12, mb: 10 }}>
        {/* Search input and filters */}
        <SearchHeader
          query={query}
          onQuery={setQuery}
          onSearch={handleSearchInput}
          filter={filter}
          onFilter={setFilter}
        />

        {/* Search content */}
        {isLoading ? (
          <LoadingNews />
        ) : (
          <SearchBody query={query} articles={queryNews} />
        )}
      </Stack>

      <Footer />
    </Container>
  );
};

const LoadingNews = () => {
  return (
    <Box
      sx={{
        width: "100%",
        height: "80vh",
        display: "grid",
        placeItems: "center",
      }}
    >
      <CircularProgress />
    </Box>
  );
};
export default MainNews;

import { Box, CircularProgress, Container, Stack } from "@mui/material";
import React, { useEffect } from "react";
import Navbar from "../components/landing/Navbar";
import SearchHeader from "../components/news/SearchHeader";
import { useState } from "react";
import SearchBody from "../components/news/SearchBody";
import { v4 as uuidv4 } from "uuid";
import { Article, Filters, initialFilters } from "@/types/news.interface";
import { useDebouncedCallback } from "use-debounce";
import Footer from "../components/landing/Footer";
import { API_LANDING } from "@/types/strings";
import MainNews from "./MainNews";

interface LandingData {
  latest: Article | undefined;
  latestNews: Article[];
}
const getLandigData = async (): Promise<LandingData> => {
  const res = await fetch(API_LANDING, { next: { revalidate: 0 } });
  if (!res.ok) {
    return res.text().then((text) => {
      throw new Error(text);
    });
  }
  const data = await res.json();
  return data;
};

const News = async () => {
  const landingData: LandingData = await getLandigData();

  return <MainNews latestNews={landingData.latestNews} />;
};

export default News;

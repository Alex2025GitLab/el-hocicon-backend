"use client";
import React, { useState } from "react";
import {
  Divider,
  Grid,
  Stack,
  Typography,
  useMediaQuery,
  Box,
  Snackbar,
  FormControl,
  Checkbox,
  FormControlLabel,
  InputLabel,
  OutlinedInput,
  FormHelperText,
  InputAdornment,
  IconButton,
  Button,
} from "@mui/material";
import { useRouter } from "next/navigation";
import { useTheme } from "@mui/material/styles";
import Link from "next/link";
import MainCard from "../components/ui/MainCard";
import { SubmitHandler, useForm } from "react-hook-form";
import {
  NewspaperOutlined,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import axios from "axios";
import { toast } from "sonner";
import { getAxiosError } from "@/utils/utils";
import { API_LOGIN, ERROR, PUBLIC, SERVER_ERROR } from "@/types/strings";

const LoginSchema = z.object({
  email: z
    .string()
    .email({
      message: "Correo inválido, por favor introduzca una dirección válida",
    })
    .min(1, "El correo es requerido"),
  password: z
    .string()
    .min(8, { message: "Debe tener al menos 8 caracteres." })
    .regex(
      /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/,
      "Debe contener al menos 1 (número, letra minúscula, letra mayúscula)"
    ),
  saveSession: z.boolean(),
});

type FormInput = z.infer<typeof LoginSchema>;

const Login = () => {
  const router = useRouter();
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down("md"));
  const [clickSubmit, setClickSubmit] = useState<boolean>(true);
  const [showPassword, setShowPassword] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormInput>({
    resolver: zodResolver(LoginSchema),
    defaultValues: {
      email: "",
      password: "",
      saveSession: false,
    },
  });
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };
  const handleMouseDownPassword = (event: React.ChangeEvent) => {
    event.preventDefault();
  };

  const onLogin: SubmitHandler<FormInput> = async (data) => {
    if (clickSubmit) {
      setClickSubmit(false);
      await axios
        .post(`${PUBLIC}/api`, data)
        .then(function (res) {
          console.log(res);
          if (res.data.message) {
            router.push(`${PUBLIC}/admin`);
          } else {
            toast.error(ERROR, {
              description: res.data.response,
            });
            setClickSubmit(true);
          }
        })
        .catch(function (error) {
          console.log(error);
          toast.error(ERROR, {
            description: SERVER_ERROR,
          });
          setClickSubmit(true);
        });
    }
  };
  return (
    <div style={{ minHeight: "100vh" }}>
      <Grid
        container
        direction="column"
        justifyContent="flex-end"
        sx={{ minHeight: "100vh" }}
      >
        <Grid item xs={12}>
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            sx={{ minHeight: "calc(100vh - 68px)" }}
          >
            <Grid item sx={{ m: { xs: 1, sm: 3 }, mb: 0 }}>
              <MainCard
                sx={{
                  maxWidth: { xs: 400, lg: 475 },
                  margin: { xs: 2.5, md: 3 },
                  "& > *": {
                    flexGrow: 1,
                    flexBasis: "50%",
                  },
                }}
                content={false}
              >
                <Box sx={{ p: { xs: 2, sm: 3, xl: 5 } }}>
                  <Grid
                    container
                    spacing={2}
                    alignItems="center"
                    justifyContent="center"
                  >
                    <Grid item sx={{ mb: 3 }}>
                      <Link href="/">
                        <Stack
                          sx={{ color: theme.palette.primary.main }}
                          direction="row"
                          alignItems="center"
                          spacing={1}
                        >
                          <NewspaperOutlined />
                          <Typography
                            variant="h4"
                            sx={{
                              fontFamily: "Merriweather",
                              fontWeight: 700,
                            }}
                          >
                            El&nbsp;Hocicón
                          </Typography>
                        </Stack>
                      </Link>
                    </Grid>

                    <Grid item xs={12}>
                      <form
                        onSubmit={handleSubmit(onLogin)}
                        style={{
                          display: "flex",
                          flexDirection: "column",
                          gap: "2rem",
                        }}
                      >
                        <FormControl fullWidth>
                          <InputLabel htmlFor="outlined-adornment-email-login">
                            Correo electrónico
                          </InputLabel>
                          <OutlinedInput
                            autoComplete="off"
                            id="outlined-adornment-email-login"
                            type="email"
                            required
                            label="Email Address / Username"
                            {...register("email")}
                          />
                          {/* {touched.email && errors.email && ( */}
                          {errors?.email?.message && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-email-login"
                            >
                              {errors.email.message}
                            </FormHelperText>
                          )}
                        </FormControl>

                        <FormControl fullWidth>
                          <InputLabel htmlFor="outlined-adornment-password-login">
                            Contraseña
                          </InputLabel>
                          <OutlinedInput
                            id="outlined-adornment-password-login"
                            type={showPassword ? "text" : "password"}
                            {...register("password")}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowPassword as any}
                                  onMouseDown={handleMouseDownPassword as any}
                                  edge="end"
                                  size="large"
                                >
                                  {showPassword ? (
                                    <Visibility />
                                  ) : (
                                    <VisibilityOff />
                                  )}
                                </IconButton>
                              </InputAdornment>
                            }
                            label="Password"
                            inputProps={{}}
                          />
                          {errors?.password?.message && (
                            <FormHelperText
                              error
                              id="standard-weight-helper-text-password-login"
                            >
                              {errors.password.message}
                            </FormHelperText>
                          )}
                        </FormControl>

                        <Stack
                          direction="row"
                          alignItems="center"
                          justifyContent="space-between"
                          spacing={1}
                        >
                          <FormControlLabel
                            control={
                              <Checkbox
                                {...register("saveSession")}
                                name="checked"
                                color="primary"
                              />
                            }
                            label="No cerrar sesión"
                          />
                          <Typography
                            color={theme.palette.primary.main}
                            variant="subtitle1"
                            sx={{ textDecoration: "none", cursor: "pointer" }}
                          >
                            ¿Olvidaste tu contraseña?
                          </Typography>
                        </Stack>

                        <Box sx={{ mt: 2 }}>
                          <Button
                            disableElevation
                            disabled={!clickSubmit}
                            fullWidth
                            size="large"
                            type="submit"
                            variant="contained"
                            color="primary"
                          >
                            Iniciar sesión
                          </Button>
                        </Box>
                      </form>
                    </Grid>

                    <Grid item xs={12}>
                      <Divider />
                    </Grid>
                    <Grid item xs={12}>
                      <Grid
                        item
                        container
                        direction="column"
                        alignItems="center"
                        xs={12}
                      >
                        <Typography
                          variant="subtitle1"
                          sx={{ textDecoration: "none" }}
                        >
                          ¿No tienes una cuenta?
                        </Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                </Box>
              </MainCard>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sx={{ m: 3, mt: 1 }}>
          <AuthFooter />
        </Grid>
      </Grid>
    </div>
  );
};

const copyright = {
  left: "© 2024",
  center: "Alexander Nina Pacajes",
  right: "aaalex2025@gmail.com",
};
const AuthFooter = () => (
  <Stack
    direction={{ xs: "column", md: "row" }}
    justifyContent="space-between"
    alignItems="center"
    spacing={1}
    sx={{ pb: 6 }}
  >
    <Typography variant="body2" color="text.secondary">
      {copyright.left}
    </Typography>

    <Typography variant="body2" color="text.secondary">
      {copyright.center}
    </Typography>

    <Typography variant="body2" color="text.secondary">
      {copyright.right}
    </Typography>
  </Stack>
);

export default Login;

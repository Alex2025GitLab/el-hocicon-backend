## **_Este es el repositorio FRONTEND de servidor (disculpe la molestia) Next._**

# Repositorio del Frontend

**Se recomienda haber ejecutado el seed y que el BACKEND ya este ejecutándose**

## Ejemplo ejecución después de realizar el seeder del BACKEND

**Video de Youtube** </br>
[<img src="https://i.ytimg.com/vi/rVQYc-FhH6c/maxresdefault.jpg" width="50%">](https://youtu.be/rVQYc-FhH6c "Ejecución código")

Frontend desarrollado sobre Next.js y Material UI se comunica con el BACKEND por medio de las APIs

Este base contiene los siguientes módulos:

- User
- News

Cuenta con creación del token JWT para middleware del lado del frontend.

## Para la instalación

Siga los pasos de **INSTALL.md** es importante tener los puertos 5432 y 3000 disponibles

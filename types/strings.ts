/* GLOBAL */
export const TOKEN = "hocicon_token";
export const PUBLIC = "http://localhost:3001";

export const API_PUBLIC = "http://localhost:3000";

/* CLIENT HANDLERS */
export const SUCCESS = "¡Éxito!";
export const ERROR = "¡Error!";
export const SERVER_ERROR = "Algo salió mal desde el servidor.";
export const WAIT = "Por favor espere...";

/* API */
export const API_LOGIN = `${API_PUBLIC}/auth/login`;
export const API_USER = `${API_PUBLIC}/users`;
export const API_NEWS = `${API_PUBLIC}/news`;
export const API_LANDING = `${API_PUBLIC}/landing`;

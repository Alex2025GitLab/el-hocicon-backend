export interface Article {
  id?: string | number;
  title: string;
  imageUrl?: string;
  publishedAt: string;
  placePublication: string;
  author: string;
  description: string;
  authorImg?: string;
}

// pasar username en creado por o editado por
export interface NewsDashboard {
  id: number;
  title: string;
  image_url: string;
  place_publication: string;
  description: string;
  content: string;
  created_at: string;
  updated_at: string;
  created_by: string;
  updated_by: string;
}

export interface FullArticle {
  title: string;
  imageUrl?: string;
  publishedAt: string;
  placePublication: string;
  author: string;
  fullContent: string;
}

export interface Filters {
  title: boolean;
  autor: boolean;
  place: boolean;
}

export const initialFilters: Filters = {
  title: false,
  autor: false,
  place: false,
};

export interface User {
  id: number;
  email: string;
  profile_img: string;
  created_at: string;
  updated_at: string;
}

export interface UserNews {
  id: number;
  email: string;
  username: string;
  profile_img: string;
}

/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: "http",
        hostname: "localhost",
        port: "3000",
      },
      {
        protocol: "https",
        hostname: "source.unsplash.com",
        port: "",
        pathname: "/random",
      },
      {
        protocol: "https",
        hostname: "cdn.mos.cms.futurecdn.net",
      },
      {
        protocol: "https",
        hostname: "techcrunch.com",
      },
      {
        protocol: "https",
        hostname: "img.thedailybeast.com",
      },
      {
        protocol: "https",
        hostname: "img.huffingtonpost.com",
      },
      {
        protocol: "https",
        hostname: "assets2.cbsnewsstatic.com",
      },
      {
        protocol: "https",
        hostname: "media.cnn.com",
      },
      {
        protocol: "https",
        hostname: "dims.apnews.com",
      },
      {
        protocol: "https",
        hostname: "a57.foxnews.com",
      },
    ],
  },
};

module.exports = nextConfig;
